//
// Created by bartek on 26.11.2019.
//

#include "Player.h"

void Player::init(Selector *s) {
    selector = s;
}

int Player::GetSelectedType() {
    return selector->GetSelected();
}

int Player::GetSelectedCount() {
    return selector->GetSelectedCount();
}

void Player::UseSelected() {
    selector->UseSelected();
}

void Player::Activate() {
    selector->Activate();
}

void Player::Deactivate() {
    selector->Deactivate();
}

Selector &Player::getSelector() {
    return *selector;
}
