//
// Created by bartek on 23.10.2019.
//

#include <TTH/Core/ComponentFactory.h>
#include "PistonButton.h"

TTH::ComponentRegister<PistonButton> PistonButton::reg("PistonButton");

void PistonButton::Start() {

}

void PistonButton::Update(float dt) {

}

void PistonButton::OnLeave() {

}

TTH::Component *PistonButton::Clone() const {
    return nullptr;
}

bool PistonButton::LoadFromFile(TTH::IniFile &file) {
    return true;
}

void PistonButton::OnMouseDown(int button) {
    if(button == 0){
        selector->Select(id);
    }
}

void PistonButton::OnMouseEnter() {
    if(!selected) {
        sc->SetColorTint(glm::vec3(1.0, 0.5, 0.0));
    }
}

void PistonButton::OnMouseLeave() {
    if(!selected) {
        sc->SetColorTint(glm::vec3(1.0, 1.0, 0.0));
    }
}

void PistonButton::OnLoad() {
    sc = parent->GetComponent<TTH::SpriteComponent>();
    selected = false;
}

void PistonButton::SetId(int _id) {
    id = _id;
}

void PistonButton::SetSelector(Selector *s) {
    selector = s;
}

void PistonButton::Select() {
    sc->SetColorTint(glm::vec3(1.0,0.0,0.0));
    selected = true;
}

void PistonButton::Deselect() {
    sc->SetColorTint(glm::vec3(1.0,1.0,0.0));
    selected = false;
}
