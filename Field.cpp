//
// Created by bartek on 22.10.2019.
//

#include <TTH/Core/ObjectFactory.h>
#include <TTH/Core/ObjectManager.h>
#include "Field.h"
#include "BoardController.h"
#include "Piston.h"

TTH::ComponentRegister<Field> Field::reg("Field");

void Field::Start() {
    rc = parent->GetComponent<TTH::RenderComponent>();
    originalColor = glm::vec3(1.0,1.0,0.0);
    highlightedColor = glm::vec3(1.0, 0.5, 0.0);
    rc->SetColor(originalColor);
    piston = nullptr;
}

void Field::Update(float dt) {

}

void Field::OnLeave() {

}

TTH::Component *Field::Clone() const {
    return nullptr;
}

bool Field::LoadFromFile(TTH::IniFile &file) {
    return true;
}

void Field::OnMouseEnter() {
    rc->SetColor(highlightedColor);
}

void Field::OnMouseLeave() {
    rc->SetColor(originalColor);
}

void Field::OnLoad() {
    Start();
}

void Field::OnMouseDown(int button) {
    if(button == 0 && piston == nullptr) {
        //BoardController::Place(num, BoardController::getActivePlayer()->GetSelectedType());
        BoardController::getInstance()->Place(num, BoardController::getInstance()->getActivePlayer()->GetSelectedType());
    }
}

glm::vec3 Field::GetPosition() {
    return parent->GetWorldTransform().position;
}
