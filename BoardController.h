//
// Created by bartek on 20.10.2019.
//

#ifndef PISTONED_BOARDCONTROLLER_H
#define PISTONED_BOARDCONTROLLER_H

#include <memory>
#include <TTH/Core/BehaviourComponent.h>
#include "Player.h"
#include "Field.h"
#include "EndTurnButton.h"
#include "MinMax.h"

class BoardController : public TTH::BehaviourComponent{
public:
    enum class Direction{
         Down = 0, Right = 1, Up = 2, Left = 3
    };
    static Direction rotateRight(Direction dir);
    static Direction rotateLeft(Direction dir);

    void Start() override;

    void Update(float dt) override;

    void OnLeave() override;

    BoardController *Clone() const override;

    Player* getActivePlayer();
    void Activate(Piston* piston);
    void Rotate(Piston* piston);
    void Place(int num, int type);
    void ActivationEnd();
    void RotationEnd();

    void PassTurn();

    enum class ActivePlayer{
        P1, P2
    };
    bool passed;

    static BoardController* getInstance();
protected:
    friend class BoardHelper;

    bool LoadFromFile(TTH::IniFile &file) override;

    void EndTurn();

    static TTH::ComponentRegister<BoardController> reg;

    Player player1, player2;
    std::vector<Field*> fields;

    EndTurnButton* endTurnButton;

    ActivePlayer activePlayer;

    enum class State{
        Idle, Animating, AITurn, AITurnExecuting
    };

    State state;
    bool aiGame;
    std::unique_ptr<MinMax> minmax;

    static BoardController* instance;

    void simulateAITurn();
};


#endif //PISTONED_BOARDCONTROLLER_H
