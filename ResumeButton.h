//
// Created by bartek on 06.12.2019.
//

#ifndef PISTONED_RESUMEBUTTON_H
#define PISTONED_RESUMEBUTTON_H


#include <TTH/Core/BehaviourComponent.h>
#include <TTH/Core/ComponentFactory.h>
#include <TTH/UI/SpriteComponent.h>

class ResumeButton : public TTH::BehaviourComponent{
public:
    Component *Clone() const override;

    void Start() override;

    void Update(float dt) override;

    void OnLeave() override;

    void OnMouseDown(int button) override;

    void OnMouseEnter() override;

    void OnMouseLeave() override;

protected:
    bool LoadFromFile(TTH::IniFile &file) override;

    TTH::SpriteComponent* sprite;
private:
    static TTH::ComponentRegister<ResumeButton> reg;
};


#endif //PISTONED_RESUMEBUTTON_H
