//
// Created by bartek on 05.12.2019.
//

#ifndef PISTONED_QUITGAMEBUTTON_H
#define PISTONED_QUITGAMEBUTTON_H


#include <TTH/Core/BehaviourComponent.h>
#include <TTH/UI/SpriteComponent.h>

class QuitGameButton : public TTH::BehaviourComponent{
public:
    void Start() override;

    void Update(float dt) override;

    void OnLeave() override;

    void OnMouseDown(int button) override;

    void OnMouseEnter() override;
    void OnMouseLeave() override;

    Component *Clone() const override;

protected:
    TTH::SpriteComponent* sprite;
    bool LoadFromFile(TTH::IniFile &file) override;

private:
    static TTH::ComponentRegister<QuitGameButton> reg;
};


#endif //PISTONED_QUITGAMEBUTTON_H
