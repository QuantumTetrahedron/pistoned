//
// Created by bartek on 06.12.2019.
//

#include <TTH/SceneManagement/SceneManager.h>
#include "ResumeButton.h"

TTH::ComponentRegister<ResumeButton> ResumeButton::reg("ResumeButton");

void ResumeButton::Start() {
    sprite = parent->GetComponent<TTH::SpriteComponent>();
}

void ResumeButton::Update(float dt) {

}

void ResumeButton::OnLeave() {

}

void ResumeButton::OnMouseDown(int button) {
    TTH::SceneManager::ResumeSceneRequest();
}

void ResumeButton::OnMouseEnter() {
    sprite->SetColorTint(glm::vec3(1.0f,0.5f,0.0f));
}

void ResumeButton::OnMouseLeave() {
    sprite->SetColorTint(glm::vec3(1.0f,1.0f,0.0f));
}

TTH::Component *ResumeButton::Clone() const {
    return nullptr;
}

bool ResumeButton::LoadFromFile(TTH::IniFile &file) {
    return true;
}
