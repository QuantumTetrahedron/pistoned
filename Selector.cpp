//
// Created by bartek on 23.10.2019.
//

#include <TTH/Core/ObjectManager.h>
#include <TTH/Core/ObjectFactory.h>
#include <TTH/Graphics/Gfx.h>
#include "Selector.h"
#include "PistonButton.h"
#include <TTH/UI/TextComponent.h>

TTH::ComponentRegister<Selector> Selector::reg("Selector");

void Selector::Start() {
    TTH::Transform wt = parent->GetWorldTransform();
    float width = wt.scale.x;
    float height = wt.scale.y;

    float innerW = width * 0.9f;
    float fixedHeight = innerW / height;

    for(int i = 0; i<4; i++){
        TTH::IniFile file;
        file.ReadFile("Archetypes/PistonButton.ini");
        TTH::ObjectFactory::RemoveArchetype();
        auto button = TTH::ObjectFactory::Create(file, parent->GetName()+"_b"+std::to_string(i));
        button->transform.scale = glm::vec3(0.9, fixedHeight, 1.0);
        button->transform.position = glm::vec3(0.5, 1.1*fixedHeight*i+0.05, 0.0);
        button->SetParent(parent);
        auto b = button->GetComponent<PistonButton>();
        b->SetId(i);
        b->SetSelector(this);
        TTH::ObjectManager::AddObject(button);
        buttons[i] = std::make_pair(b, i == 0 ? 5 : 1);

        TTH::IniFile f2;
        f2.ReadFile("Archetypes/CounterText.ini");
        auto text = TTH::ObjectFactory::Create(f2, parent->GetName()+"_bText"+std::to_string(i));

        auto tt = text->GetComponent<TTH::TextComponent>();
        tt->SetText(std::to_string(buttons[i].second));
        text->SetParent(button);

        b->countText = tt;
        TTH::ObjectManager::AddObject(text);

        TTH::IniFile f3;
        f3.ReadFile("Archetypes/Sprite.ini");
        auto spriteObj = TTH::ObjectFactory::Create(f3, parent->GetName()+"_bSprite"+std::to_string(i));
        auto sprite = spriteObj->GetComponent<TTH::SpriteComponent>();
        spriteObj->SetParent(button);
        sprite->SetTexture("Piston"+std::to_string(i)+player);
        TTH::ObjectManager::AddObject(spriteObj);
    }
    //selected = -1;
    Select(0);
}

void Selector::Update(float dt) {

}

void Selector::OnLeave() {

}

Selector *Selector::Clone() const {
    return nullptr;
}

bool Selector::LoadFromFile(TTH::IniFile &file) {
    file.SetToSection("Selector");
    file.RequireValue("player", player);
    return true;
}

void Selector::Select(int id) {
    selected = id;
    for(auto b : buttons){
        if(b.first->id == id){
            b.first->Select();
        } else {
            b.first->Deselect();
        }
    }
}

int Selector::GetSelected() {
    return selected;
}

int Selector::GetSelectedCount() {
    for(auto b:buttons){
        if(b.first->id == selected){
            return b.second;
        }
    }
    return -1;
}

void Selector::UseSelected() {
    for(auto& b:buttons){
        if(b.first->id == selected){
            if(b.second > 0){
                b.second--;
                b.first->countText->SetText(std::to_string(b.second));
            }
        }
    }
}

void Selector::Activate() {
    parent->GetComponent<TTH::SpriteComponent>()->SetColorTint(glm::vec3(0.8f,0.1f,0.0f));
}

void Selector::Deactivate() {
    parent->GetComponent<TTH::SpriteComponent>()->SetColorTint(glm::vec3(0.5f, 0.2f, 0.0f));
}
