//
// Created by magda on 08.12.2019.
//

#include <gtest/gtest.h>
#include <TTH/Core/Game.h>
#include <TTH/Core/ObjectFactory.h>
#include <TTH/Core/ObjectManager.h>
#include "../Piston.h"
#include "BoardHelper.h"

namespace test {
    class BoardPlacingTest : public ::testing::Test {
    protected:
        void SetUp() override {
            TTH::Game::Initialize("GameData/EngineOptions.ini");
            TTH::IniFile boardFile;
            boardFile.ReadFile("Scenes/Game/Board.ini");
            board = TTH::ObjectFactory::Create(boardFile, "board");
            boardFile.ReadFile("Archetypes/Selector.ini");
            TTH::ObjectFactory::SetArchetype(boardFile);
            boardFile.ReadFile("Scenes/Game/P1Selector.ini");
            selector1 = TTH::ObjectFactory::Create(boardFile, "P1Selector");

            boardFile.ReadFile("Scenes/Game/P2Selector.ini");
            selector2 = TTH::ObjectFactory::Create(boardFile, "P2Selector");

            TTH::ObjectFactory::RemoveArchetype();

            boardFile.ReadFile("Scenes/Game/PassButton.ini");
            passButton = TTH::ObjectFactory::Create(boardFile, "PassButton");

            TTH::ObjectManager::AddObject(board);
            TTH::ObjectManager::AddObject(selector1);
            TTH::ObjectManager::AddObject(selector2);
            TTH::ObjectManager::AddObject(passButton);

            startAllComponents(passButton);
            startAllComponents(selector1);
            startAllComponents(selector2);
            startAllComponents(board);
        }

        static void startAllComponents(TTH::Object* object) {
            auto comps = object->GetAllComponents<TTH::BehaviourComponent>();
            //std::cout << comps.size() << std::endl;
            std::for_each(comps.begin(), comps.end(), [](const auto& c){ c->Start(); });
        }

        void TearDown() override {
            TTH::Game::Shutdown();
        }

        TTH::Object* passButton = nullptr;
        TTH::Object* selector1 = nullptr;
        TTH::Object* selector2 = nullptr;
        TTH::Object* board = nullptr;
    };


TEST_F(BoardPlacingTest, CanPlacePiston) {
    auto boardComp = board->GetComponent<BoardController>();
    boardComp->Place(10, 0);
    Field* field = BoardHelper::getFields(*boardComp)[10];
    ASSERT_TRUE(field->hasPiston);
    EXPECT_EQ(field->piston->type, Piston::Type::Front);
}

TEST_F(BoardPlacingTest, CanPlacePistonForCorrectPlayer) {
    auto boardComp = board->GetComponent<BoardController>();
    auto fields = BoardHelper::getFields(*boardComp);
    auto firstPlayer = BoardHelper::getActivatedPlayer(*boardComp);

    boardComp->Place(1, 0);

    ASSERT_TRUE(fields[1]->hasPiston);
    EXPECT_EQ(fields[1]->piston->owner, firstPlayer);

    auto secondPlayer = BoardHelper::getActivatedPlayer(*boardComp);
    EXPECT_NE(firstPlayer, secondPlayer);

    boardComp->Place(2, 0);
    ASSERT_TRUE(fields[2]->hasPiston);
    EXPECT_EQ(fields[2]->piston->owner, secondPlayer);
}

TEST_F(BoardPlacingTest, IgnoresOverridingFields) {
    auto boardComp = board->GetComponent<BoardController>();
    auto fields = BoardHelper::getFields(*boardComp);

    auto firstPlayer = BoardHelper::getActivatedPlayer(*boardComp);
    // First player places a piston somewhere
    boardComp->Place(5, 0);
    ASSERT_TRUE(fields[5]->hasPiston);
    EXPECT_EQ(fields[5]->piston->owner, firstPlayer);

    auto secondPlayer = BoardHelper::getActivatedPlayer(*boardComp);
    // Second player wants to place on the same field
    boardComp->Place(5, 1);
    // But the field is still occupied by someone else
    EXPECT_NE(fields[5]->piston->owner, secondPlayer);
    // And it's still 2nd player's turn
    EXPECT_EQ(BoardHelper::getActivatedPlayer(*boardComp), secondPlayer);
}

TEST_F(BoardPlacingTest, CanPlaceDifferentTypesOfPistons) {
    auto boardComp = board->GetComponent<BoardController>();
    auto fields = BoardHelper::getFields(*boardComp);
    boardComp->Place(10, 0);
    boardComp->Place(12, 1);
    ASSERT_TRUE(fields[10]->hasPiston);
    ASSERT_TRUE(fields[12]->hasPiston);
    EXPECT_NE(fields[12]->piston->type, fields[10]->piston->type);
}

TEST_F(BoardPlacingTest, PlacingAffectsSelectorState) {
    auto boardComp = board->GetComponent<BoardController>();
    auto fields = BoardHelper::getFields(*boardComp);
    auto firstPlayer = BoardHelper::getActivatedPlayer(*boardComp);
    auto selector = selector1->GetComponent<Selector>();
    // Player selects piston type
    selector->Select(1);
    int countInSelector = selector->GetSelectedCount();

    // this should place the piston
    boardComp->Place(1, selector->GetSelected());
    ASSERT_TRUE(fields[1]->hasPiston);
    EXPECT_EQ(fields[1]->piston->owner, firstPlayer);
    EXPECT_EQ(fields[1]->piston->type, Piston::Type::FrontRight);
    EXPECT_EQ(selector->GetSelectedCount(), countInSelector - 1);
}

TEST_F(BoardPlacingTest, IgnoresPlacingFromEmptySelector) {
    auto boardComp = board->GetComponent<BoardController>();
    auto fields = BoardHelper::getFields(*boardComp);
    auto firstPlayer = BoardHelper::getActivatedPlayer(*boardComp);
    auto selector = selector1->GetComponent<Selector>();

    // Remove pistons of a type
    selector->buttons[0].second = 0;

    // Player selects piston type
    selector->Select(0);
    int countInSelector = selector->GetSelectedCount();
    EXPECT_EQ(countInSelector, 0);

    // Player places on an empty field (this should do nothing)
    boardComp->Place(1, selector->GetSelected());
    EXPECT_FALSE(fields[1]->hasPiston);
    EXPECT_EQ(selector->GetSelectedCount(), countInSelector);
    EXPECT_EQ(BoardHelper::getActivatedPlayer(*boardComp), firstPlayer);
}

}