//
// Created by magda on 08.12.2019.
//

#ifndef PISTONED_BOARDHELPER_H
#define PISTONED_BOARDHELPER_H


#include "../Field.h"
#include "../BoardController.h"

class BoardHelper {
public:
    static std::vector<Field*> getFields(const BoardController& board);
    static bool isIdle(const BoardController& board);
    static bool isAnimating(const BoardController& board);
    static bool isAITurn(const BoardController& board);
    static bool isAITurnExecuting(const BoardController& board);
    static BoardController::ActivePlayer getActivatedPlayer(const BoardController& board);
    static void setMockMinMax(BoardController* board, std::unique_ptr<MinMax> minMax);
    static void simulateAI(BoardController* board);
};


#endif //PISTONED_BOARDHELPER_H
