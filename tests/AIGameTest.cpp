//
// Created by magda on 08.12.2019.
//

#include <gtest/gtest.h>
#include <TTH/Core/Object.h>
#include <TTH/Core/ObjectManager.h>
#include <TTH/Core/ObjectFactory.h>
#include <TTH/Core/Game.h>

#include <utility>
#include "../MinMax.h"
#include "BoardHelper.h"
#include "../Piston.h"

namespace test {
class MockMinMax : public MinMax {
public:
    explicit MockMinMax(std::pair<int, int> mockResult) : MinMax(2), mockResult(std::move(mockResult)) {}

    std::pair<int, int> GetMove(const std::vector<Field *> &state, int passCounter, const Selector &selector1,
                                const Selector &selector2) override {
        return mockResult;
    }

protected:
    std::pair<int, int> mockResult;
};

class AIGameTest : public ::testing::Test {
protected:
    void SetUp() override {
        TTH::Game::Initialize("GameData/EngineOptions.ini");
        TTH::IniFile boardFile;
        boardFile.ReadFile("Scenes/AIGame/Board.ini");
        board = TTH::ObjectFactory::Create(boardFile, "board");
        boardFile.ReadFile("Archetypes/Selector.ini");
        TTH::ObjectFactory::SetArchetype(boardFile);
        boardFile.ReadFile("Scenes/AIGame/P1Selector.ini");
        selector1 = TTH::ObjectFactory::Create(boardFile, "P1Selector");

        boardFile.ReadFile("Scenes/AIGame/P2Selector.ini");
        selector2 = TTH::ObjectFactory::Create(boardFile, "P2Selector");

        TTH::ObjectFactory::RemoveArchetype();

        boardFile.ReadFile("Scenes/AIGame/PassButton.ini");
        passButton = TTH::ObjectFactory::Create(boardFile, "PassButton");

        TTH::ObjectManager::AddObject(board);
        TTH::ObjectManager::AddObject(selector1);
        TTH::ObjectManager::AddObject(selector2);
        TTH::ObjectManager::AddObject(passButton);

        startAllComponents(passButton);
        startAllComponents(selector1);
        startAllComponents(selector2);
        startAllComponents(board);
    }

    static void startAllComponents(TTH::Object* object) {
        auto comps = object->GetAllComponents<TTH::BehaviourComponent>();
        //std::cout << comps.size() << std::endl;
        std::for_each(comps.begin(), comps.end(), [](const auto& c){ c->Start(); });
    }

    void TearDown() override {
        TTH::Game::Shutdown();
    }

    TTH::Object* passButton = nullptr;
    TTH::Object* selector1 = nullptr;
    TTH::Object* selector2 = nullptr;
    TTH::Object* board = nullptr;
};

TEST_F(AIGameTest, CanPassTurn) {
    auto boardComp = board->GetComponent<BoardController>();
    auto fields = BoardHelper::getFields(*boardComp);
    auto mock = MockMinMax(std::make_pair<int, int>(-1, 0));
    BoardHelper::setMockMinMax(boardComp, std::make_unique<MockMinMax>(mock));

    boardComp->Place(0,1);
    BoardHelper::simulateAI(boardComp);

    EXPECT_TRUE(fields[0]->hasPiston);

    for (int i = 1; i < fields.size(); ++i) {
        EXPECT_FALSE(fields[i]->hasPiston);
    }
}

TEST_F(AIGameTest, CanPlaceNewPistonAndModifySelector) {
    auto boardComp = board->GetComponent<BoardController>();
    auto fields = BoardHelper::getFields(*boardComp);
    auto mock = MockMinMax(std::make_pair<int, int>(2, 3));

    BoardHelper::setMockMinMax(boardComp, std::make_unique<MockMinMax>(mock));

    auto selector = selector2->GetComponent<Selector>();
    int countInSelector = selector->buttons[3].second;

    boardComp->Place(0,1);
    BoardHelper::simulateAI(boardComp);

    ASSERT_TRUE(fields[0]->hasPiston);
    ASSERT_TRUE(fields[2]->hasPiston);
    EXPECT_EQ(fields[2]->piston->owner, BoardController::ActivePlayer::P2);
    EXPECT_EQ(fields[2]->piston->type, Piston::Type::None);
    EXPECT_EQ(selector->GetSelected(), 3);
    EXPECT_EQ(selector->GetSelectedCount(), countInSelector - 1);
}

TEST_F(AIGameTest, CanPushPistons) {
    auto boardComp = board->GetComponent<BoardController>();
    auto fields = BoardHelper::getFields(*boardComp);
    auto mock = MockMinMax(std::make_pair<int, int>(2, 3));
    BoardHelper::setMockMinMax(boardComp, std::make_unique<MockMinMax>(mock));

    boardComp->Place(1,1);
    boardComp->Place(2, 0);
    boardComp->PassTurn();

    EXPECT_TRUE(fields[1]->hasPiston);
    EXPECT_TRUE(fields[2]->hasPiston);

    BoardHelper::simulateAI(boardComp);

    EXPECT_TRUE(fields[0]->hasPiston);
    EXPECT_TRUE(fields[2]->hasPiston);
}

}