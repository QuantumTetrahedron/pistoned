//
// Created by magda on 07.12.2019.
//

#include <gtest/gtest.h>
#include <TTH/Core/Game.h>
#include <TTH/Core/ObjectManager.h>
#include <TTH/Core/ObjectFactory.h>
#include "../Piston.h"

namespace test {
    class PistonTest : public ::testing::Test {
    protected:
        void SetUp() override {
            TTH::Game::Initialize("GameData/EngineOptions.ini");
            TTH::IniFile file;
            obj = TTH::ObjectFactory::Create(file, "object");
            Piston* piston = new Piston();
            piston->type = Piston::Type::None;

            obj->AddComponent(piston);
            piston->InitArms();

            TTH::ObjectManager::AddObject(obj);
        }

        void TearDown() override {
            TTH::Game::Shutdown();
        }

        TTH::Object* obj = nullptr;
    };

    TEST_F(PistonTest, IsRotating) {
        Piston* piston = obj->GetComponent<Piston>();
        float oldRotation = obj->transform.rotation.y;
        piston->Rotate();
        EXPECT_EQ(obj->transform.rotation.y, oldRotation);
        EXPECT_EQ(piston->getTargetRotation(), glm::mod(oldRotation + 90.0f, 360.0f));
    }
}

