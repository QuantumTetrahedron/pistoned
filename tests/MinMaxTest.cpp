//
// Created by magda on 07.12.2019.
//

#include <gtest/gtest.h>
#include <TTH/Core/Game.h>
#include "../Piston.h"
#include "../MinMax.h"

namespace test {
    class MinMaxTest : public ::testing::Test {
    protected:
        void SetUp() override {
            TTH::Game::Initialize("GameData/EngineOptions.ini");
        }

        void TearDown() override {
            TTH::Game::Shutdown();
        }

        MinMax minMax = MinMax(2);
    };

    TEST_F(MinMaxTest, IsPassingWhenWinningAndOpponentPassed) {
        int passCounter = 1;

        // Example board - P2 has more elements (even numbers)
        std::vector<int> state = {0, 0, 0, 2,
                                  0, 0, 1, 0,
                                  4, 0, 8, 0,
                                  2, 2, 0, 1};

        std::vector<int> mockSelector = {2, 1, 0, 0, 0, 0, 0, 0};

        EXPECT_EQ(std::get<0>(minMax.GetMoveInternal(state, passCounter, mockSelector)), -1);
    }

    TEST_F(MinMaxTest, IsPassingWhenNothingElsePossible) {
        int passCounter = 0;

        // Example board - none of the pistons can be activated
        std::vector<int> state = {0, 0, 0, 2,
                                  0, 0, 1, 0,
                                  4, 0, 8, 0,
                                  0, 0, 0, 7};

        // P2 has no more pistons to place
        std::vector<int> mockSelector = {2, 0, 1, 0, 1, 0, 1, 0};

        EXPECT_EQ(std::get<0>(minMax.GetMoveInternal(state, passCounter, mockSelector)), -1);
    }

    TEST_F(MinMaxTest, IsThrowingAwayOpponentPistons) {
        int passCounter = 0;

        std::vector<int> state = {0, 0, 2, 1,
                                  0, 0, 1, 0,
                                  0, 0, 8, 0,
                                  0, 2, 0, 1};

        std::vector<int> mockSelector = {0, 1, 0, 0, 0, 0, 0, 0};

        auto result = minMax.GetMoveInternal(state, passCounter, mockSelector);

        // Will activate piston at state[2]
        EXPECT_EQ(std::get<0>(result), 2);

        // Will be directed right
        EXPECT_EQ(std::get<1>(result), (int) BoardController::Direction::Right);
    }
}

