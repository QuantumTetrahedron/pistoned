//
// Created by magda on 08.12.2019.
//

#include "BoardHelper.h"

std::vector<Field *> BoardHelper::getFields(const BoardController &board) {
    return board.fields;
}

bool BoardHelper::isIdle(const BoardController &board) {
    return board.state == BoardController::State::Idle;
}

bool BoardHelper::isAnimating(const BoardController &board) {
    return board.state == BoardController::State::Animating;
}

bool BoardHelper::isAITurn(const BoardController &board) {
    return board.state == BoardController::State::AITurn;
}

bool BoardHelper::isAITurnExecuting(const BoardController &board) {
    return board.state == BoardController::State::AITurnExecuting;
}

BoardController::ActivePlayer BoardHelper::getActivatedPlayer(const BoardController &board) {
    return board.activePlayer;
}

void BoardHelper::setMockMinMax(BoardController *board, std::unique_ptr<MinMax> minMax) {
    board->minmax = std::move(minMax);
}

void BoardHelper::simulateAI(BoardController* board) {
    board->simulateAITurn();
}
