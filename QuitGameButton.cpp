//
// Created by bartek on 05.12.2019.
//

#include <TTH/Input/Input.h>
#include "QuitGameButton.h"

TTH::ComponentRegister<QuitGameButton> QuitGameButton::reg("QuitGameButton");

void QuitGameButton::Start() {
    sprite = parent->GetComponent<TTH::SpriteComponent>();
}

void QuitGameButton::Update(float dt) {

}

void QuitGameButton::OnLeave() {

}

void QuitGameButton::OnMouseDown(int button) {
    TTH::Input::QuitGame();
}

void QuitGameButton::OnMouseEnter() {
    sprite->SetColorTint(glm::vec3(1.0f,0.5f,0.0f));
}

void QuitGameButton::OnMouseLeave() {
    sprite->SetColorTint(glm::vec3(1.0f,1.0f,0.0f));
}

TTH::Component *QuitGameButton::Clone() const {
    return nullptr;
}

bool QuitGameButton::LoadFromFile(TTH::IniFile &file) {
    return true;
}
