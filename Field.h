//
// Created by bartek on 22.10.2019.
//

#ifndef PISTONED_FIELD_H
#define PISTONED_FIELD_H


#include <TTH/Core/BehaviourComponent.h>
#include <TTH/Core/ComponentFactory.h>
#include <TTH/Graphics/RenderComponent.h>

class Piston;
class Field : public TTH::BehaviourComponent{
public:
    void Start() override;

    int num;
protected:
    void OnLoad() override;

public:
    void Update(float dt) override;

    void OnLeave() override;

    Component *Clone() const override;

    void OnMouseEnter() override;
    void OnMouseLeave() override;

    void OnMouseDown(int button) override;

    bool hasPiston;

    Piston* piston;

    glm::vec3 GetPosition();
protected:
    bool LoadFromFile(TTH::IniFile &file) override;

private:
    TTH::RenderComponent* rc;
    glm::vec3 originalColor;
    glm::vec3 highlightedColor;

    static TTH::ComponentRegister<Field> reg;
};


#endif //PISTONED_FIELD_H
