//
// Created by bartek on 05.12.2019.
//

#include <TTH/SceneManagement/SceneManager.h>
#include <TTH/UI/SpriteComponent.h>
#include <TTH/Core/ComponentFactory.h>
#include "ChangeSceneButton.h"

TTH::ComponentRegister<ChangeSceneButton> ChangeSceneButton::reg("ChangeSceneButton");

void ChangeSceneButton::Start() {
    sprite = parent->GetComponent<TTH::SpriteComponent>();
}

void ChangeSceneButton::Update(float dt) {

}

void ChangeSceneButton::OnLeave() {

}

void ChangeSceneButton::OnMouseDown(int button) {
    TTH::SceneManager::LoadNextSceneRequest(scene);
}

void ChangeSceneButton::OnMouseEnter() {
    sprite->SetColorTint(glm::vec3(1.0f,0.5f,0.0f));
}

void ChangeSceneButton::OnMouseLeave() {
    sprite->SetColorTint(glm::vec3(1.0f,1.0f,0.0f));
}

TTH::Component *ChangeSceneButton::Clone() const {
    return nullptr;
}

bool ChangeSceneButton::LoadFromFile(TTH::IniFile &file) {
    file.SetToSection("ChangeSceneButton");
    file.RequireValue("scene", scene);
    return true;
}
