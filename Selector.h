//
// Created by bartek on 23.10.2019.
//

#ifndef PISTONED_SELECTOR_H
#define PISTONED_SELECTOR_H


#include <TTH/Core/BehaviourComponent.h>
#include <TTH/Core/ComponentFactory.h>

class PistonButton;
class Selector : public TTH::BehaviourComponent{
public:
    void Start() override;

    void Update(float dt) override;

    void OnLeave() override;

    Selector *Clone() const override;

    void Select(int id);
    int GetSelected();
    int GetSelectedCount();
    void UseSelected();

    void Activate();
    void Deactivate();

    std::pair<PistonButton*, int> buttons[4];
    int selected;
protected:
    bool LoadFromFile(TTH::IniFile &file) override;

    std::string player;

private:
    static TTH::ComponentRegister<Selector> reg;

};


#endif //PISTONED_SELECTOR_H
