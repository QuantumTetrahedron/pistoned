//
// Created by bartek on 23.10.2019.
//

#ifndef PISTONED_PISTONBUTTON_H
#define PISTONED_PISTONBUTTON_H

#include <TTH/UI/SpriteComponent.h>
#include <TTH/Core/BehaviourComponent.h>
#include <TTH/UI/TextComponent.h>
#include "Selector.h"

class PistonButton : public TTH::BehaviourComponent{
public:
    void Start() override;

    void OnMouseDown(int button) override;

    void OnMouseEnter() override;

    void OnMouseLeave() override;

    void Update(float dt) override;

    void OnLeave() override;

    void Select();
    void Deselect();

    void SetId(int _id);
    int id;
    void SetSelector(Selector* s);

    TTH::TextComponent* countText;

protected:
    Component *Clone() const override;

    bool LoadFromFile(TTH::IniFile &file) override;

    void OnLoad() override;

private:
    TTH::SpriteComponent* sc;

    static TTH::ComponentRegister<PistonButton> reg;

    Selector* selector;
    bool selected;
};


#endif //PISTONED_PISTONBUTTON_H
