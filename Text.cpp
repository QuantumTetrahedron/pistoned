//
// Created by bartek on 06.12.2019.
//

#include "Text.h"

float Text::scale = 1.0f;

TTH::ComponentRegister<Text> Text::reg("Text");

void Text::Start() {
    parent->transform.scale.x = scale;
    parent->transform.scale.y = scale;
    currScale = scale;
}

void Text::Update(float dt) {
    if(currScale != scale){
        parent->transform.scale.x = scale;
        parent->transform.scale.y = scale;
        currScale = scale;
    }
}

void Text::OnLeave() {

}

TTH::Component *Text::Clone() const {
    return nullptr;
}

bool Text::LoadFromFile(TTH::IniFile &file) {
    return true;
}
