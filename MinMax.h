//
// Created by magda on 04.12.2019.
//

#ifndef PISTONED_MINMAX_H
#define PISTONED_MINMAX_H


typedef std::tuple<int, int, double> MinMaxMove;

class Field;
class Selector;

class MinMax {
public:
    MinMax(int player) : playerCode(player) {}
    virtual std::pair<int, int> GetMove(const std::vector<Field*>& state, int passCounter,
            const Selector& selector1, const Selector& selector2);

    std::pair<int, int> GetMoveInternal(std::vector<int> state, int passCounter, std::vector<int> selector);
    double alphabeta(std::vector<int> state, int d, int passCounter,
                     std::vector<int> selector, double alpha, double beta, bool maximizing);
    int EvaluateExt(const std::vector<int>& state, const std::vector<int>& selector);
    int Evaluate(const std::vector<int>& state, const std::vector<int>& selector, int d);
    int EvaluateEnd(const std::vector<int>& state, const std::vector<int>& selector);
    std::pair<std::vector<int>, bool> Mov(const std::vector<int>& state, int field, int rot);

protected:
    int depth = 5;
    int counter = 0;
    int maxCounter = 3;
    int playerCode;
};


#endif //PISTONED_MINMAX_H
