//
// Created by bartek on 26.11.2019.
//

#ifndef PISTONED_PLAYER_H
#define PISTONED_PLAYER_H


#include "Selector.h"

class Player {
public:

    void init(Selector* s);

    int GetSelectedType();
    int GetSelectedCount();
    void UseSelected();

    void Activate();
    void Deactivate();

    Selector& getSelector();
private:
    Selector* selector;
};


#endif //PISTONED_PLAYER_H
