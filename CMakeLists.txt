cmake_minimum_required(VERSION 3.13)
project(Pistoned)
find_package(glfw3 REQUIRED)

set(CMAKE_CXX_STANDARD 17)

add_executable(Pistoned main.cpp BoardController.cpp BoardController.h Field.cpp Field.h Piston.cpp Piston.h Selector.cpp Selector.h PistonButton.cpp PistonButton.h Player.cpp Player.h EndTurnButton.cpp EndTurnButton.h MinMax.cpp MinMax.h ChangeSceneButton.cpp ChangeSceneButton.h QuitGameButton.cpp QuitGameButton.h StartGameButton.cpp StartGameButton.h ChangeResolutionButton.cpp ChangeResolutionButton.h Text.cpp Text.h ResumeButton.cpp ResumeButton.h )

add_executable(PistonedTest BoardController.cpp BoardController.h Field.cpp Field.h Piston.cpp Piston.h Selector.cpp Selector.h PistonButton.cpp PistonButton.h Player.cpp Player.h EndTurnButton.cpp EndTurnButton.h MinMax.cpp MinMax.h ChangeSceneButton.cpp ChangeSceneButton.h QuitGameButton.cpp QuitGameButton.h StartGameButton.cpp StartGameButton.h ChangeResolutionButton.cpp ChangeResolutionButton.h Text.cpp Text.h ResumeButton.cpp ResumeButton.h tests/PistonTest.cpp tests/MinMaxTest.cpp tests/BoardPlacingTest.cpp tests/BoardHelper.cpp tests/BoardHelper.h tests/AIGameTest.cpp)
#add_executable(PistonedTest tests/PistonTest.cpp MinMax.cpp MinMax.h)

target_link_libraries(Pistoned TheTetrahedron dl)
target_link_libraries(PistonedTest TheTetrahedron gtest gtest_main dl -lpthread -lm)

target_link_directories(Pistoned PRIVATE /usr/local/lib)
target_include_directories(Pistoned PRIVATE include)
target_include_directories(Pistoned PRIVATE src)
target_link_directories(PistonedTest PRIVATE /usr/local/lib)
target_include_directories(PistonedTest PRIVATE include)
target_include_directories(PistonedTest PRIVATE src)

file(COPY Archetypes DESTINATION .)
file(COPY GameData DESTINATION .)
file(COPY Models DESTINATION .)
file(COPY Scenes DESTINATION .)
file(COPY Shaders DESTINATION .)
file(COPY Textures DESTINATION .)
file(COPY Fonts DESTINATION .)


enable_testing()