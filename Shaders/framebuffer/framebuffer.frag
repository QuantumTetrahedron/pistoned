#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D screenTexture;

void main()
{
    FragColor = texelFetch(screenTexture, ivec2(TexCoords),0);
}