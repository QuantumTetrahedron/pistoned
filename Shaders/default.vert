#version 330 core

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTexCoords;
layout(location = 2) in vec3 vNormal;

out vec3 FragPos;
out vec2 TexCoords;

uniform mat4 model;
uniform mat4 projectionView;

out vec3 normal;

void main(){
    FragPos = vec3(model*vec4(vPosition, 1.0));
    TexCoords = vTexCoords;

    normal = normalize(transpose(inverse(mat3(model))) * vNormal);


    gl_Position = projectionView * model * vec4(vPosition, 1.0);
}