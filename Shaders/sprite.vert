#version 330 core

layout(location = 0) in vec2 vPosition;

out vec2 TexCoords;

uniform mat4 model;
uniform mat4 projection;

void main(){
    TexCoords = vPosition;
    gl_Position = projection * model * vec4(vPosition.x, 1.0f-vPosition.y, 0.0f, 1.0f);
}