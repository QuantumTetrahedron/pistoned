#version 330 core

in vec3 FragPos;
in vec2 TexCoords;

out vec4 fragColor;

uniform vec3 colorTint;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_specular1;
//uniform sampler2D texture_normal1;

struct Light{
    vec3 position;
    vec3 color;
};

uniform Light sun;
uniform vec3 viewPos;

in vec3 normal;

vec3 CalcLight(Light light, float mat_ambient, vec3 mat_diffuse, float mat_specular){

    //vec3 lightDir = normalize(light.position - FragPos);
    vec3 lightDir = normalize(light.position);
    float diffuse = max(dot(lightDir, normal), 0.0);

    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float specular = mat_specular * pow(max(dot(normal, halfwayDir), 0.0), 32.0) * max(sign(diffuse),0.0);

    vec3 outColor = (mat_ambient + diffuse + specular) * mat_diffuse * light.color;
    return outColor;
}

void main(){
    float material_ambient = 0.01;
    vec3 material_diffuse = texture( texture_diffuse1, TexCoords ).rgb * colorTint;
    float material_specular = texture( texture_specular1, TexCoords ).r;

    //vec3 material_normal = texture( texture_normal1, TexCoords ).rgb;
    //material_normal = normalize(material_normal*2.0-1.0);

    //vec3 outColor = CalcLight(Sun, material_ambient, material_diffuse, material_specular, material_normal);
    vec3 outColor = CalcLight(sun, material_ambient, material_diffuse, material_specular);
    //vec3 outColor = material_diffuse;
    fragColor = vec4(outColor, 1.0);
}