#version 330 core

out vec4 fragColor;

uniform vec3 colorTint;
uniform sampler2D tex;
in vec2 TexCoords;

void main(){
    fragColor = texture(tex, TexCoords) * vec4(colorTint,1.0);
}