//
// Created by bartek on 05.12.2019.
//

#ifndef PISTONED_STARTGAMEBUTTON_H
#define PISTONED_STARTGAMEBUTTON_H


#include <TTH/Core/BehaviourComponent.h>
#include <TTH/Core/ComponentFactory.h>
#include <TTH/UI/SpriteComponent.h>

class StartGameButton : public TTH::BehaviourComponent {
public:
    Component *Clone() const override;

    void Start() override;

    void Update(float dt) override;

    void OnLeave() override;

    void OnMouseDown(int button) override;

    void OnMouseEnter() override;

    void OnMouseLeave() override;
protected:
    TTH::SpriteComponent* sprite;
    bool LoadFromFile(TTH::IniFile &file) override;
    bool aiGame;
private:
    static TTH::ComponentRegister<StartGameButton> reg;
};


#endif //PISTONED_STARTGAMEBUTTON_H
