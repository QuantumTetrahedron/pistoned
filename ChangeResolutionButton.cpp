//
// Created by bartek on 06.12.2019.
//

#include <TTH/Input/Input.h>
#include <TTH/Core/ObjectManager.h>
#include <TTH/Core/CameraComponent.h>
#include "ChangeResolutionButton.h"
#include "Text.h"

TTH::ComponentRegister<ChangeResolutionButton> ChangeResolutionButton::reg("ChangeResolutionButton");

void ChangeResolutionButton::Start() {
    sprite = parent->GetComponent<TTH::SpriteComponent>();
}

void ChangeResolutionButton::Update(float dt) {

}

void ChangeResolutionButton::OnLeave() {
}

void ChangeResolutionButton::OnMouseDown(int button) {
    TTH::Input::SetResolution(resolution.x, resolution.y);
    TTH::ObjectManager::GetObject("Camera")->GetComponent<TTH::CameraComponent>()->SetResolution(resolution.x,resolution.y);
    Text::scale = textScale;
    TTH::Input::SetFullscreen(fullscreen);
}

void ChangeResolutionButton::OnMouseEnter() {
    sprite->SetColorTint(glm::vec3(1.0f,0.5f,0.0f));
}

void ChangeResolutionButton::OnMouseLeave() {
    sprite->SetColorTint(glm::vec3(1.0f,1.0f,0.0f));
}

TTH::Component *ChangeResolutionButton::Clone() const {
    return nullptr;
}

bool ChangeResolutionButton::LoadFromFile(TTH::IniFile &file) {
    file.SetToSection("ChangeResolutionButton");
    file.RequireValue("resolution", resolution);
    file.RequireValue("fullscreen", fullscreen);
    file.RequireValue("textScale", textScale);
    return true;
}
