//
// Created by magda on 04.12.2019.
//

#include <algorithm>
#include <random>
#include "MinMax.h"
#include "Piston.h"

/*
 * Internal state of the board - vector of 16 ints with values:
 * 0 - empty
 * 1, 3, 5, 7 - P1's pistons (Front, FrontRight, FrontBack, None)
 * 2, 4, 6, 8 - P2's pistons (as above)
 */

std::vector<int> GenerateInternalState(const std::vector<Field *> &state) {
    std::vector<int> st;
    for (const auto& field : state) {
        if (field == nullptr || field->piston == nullptr) {
            st.push_back(0);
            continue;
        }
        int value = (int) field->piston->type * 2 + 1;
        if (field->piston->owner == BoardController::ActivePlayer::P2) {
            value += 1;
        }
        st.push_back(value);
    }
    return st;
}

/*
 * Interal representation of both Selectors - 8 ints with piston counts:
 * [0] P1 Front
 * [1] P2 Front
 * [2] P1 FrontRight
 * [3] P2 FrontRight
 * [4] P1 FrontBack
 * [5] P2 FrontBack
 * [6] P1 Solid
 * [7] P2 Solid
 */

std::vector<int> GenerateInternalSelector(const Selector &selector1, const Selector &selector2) {
    std::vector<int> selector;
    for (int i = 0; i<4; ++i) {
        selector.push_back(selector1.buttons[i].second);
        selector.push_back(selector2.buttons[i].second);
    }
    return selector;
}

/*
 * Return value - pair:
 * First - type of move (-1 : pass, 0-15 : used field)
 * Second - ignored if first == -1; if field[first] is occupied, describes the rotation of activated piston.
 *   Otherwise, it specifies the type of piston to be placed at field[first] (Front, FrontRight, FrontBack, None/Solid).
 */

std::pair<int, int> MinMax::GetMove(const std::vector<Field *> &state, int passCounter, const Selector &selector1,
                                    const Selector &selector2) {
    return GetMoveInternal(GenerateInternalState(state), passCounter,
                           GenerateInternalSelector(selector1, selector2));
}

std::pair<int, int> MinMax::GetMoveInternal(std::vector<int> state, int passCounter, std::vector<int> selector) {
    std::vector<MinMaxMove> arr;

    for (int i = 0; i < 16; i++) {
        for (int j = 0; j<4; j++) {
            if (state[i] == 0) {
                // This field is empty.
                int x = j*2 + playerCode - 1;
                if (selector[x] > 0) {
                    // You have pistons of |j| type.
                    state[i] = x+1;
                    selector[x] -= 1;
                    arr.emplace_back(i, j, alphabeta(state,
                            1, 0, selector, -100000, 100000, false));
                    selector[x] += 1;
                    state[i] = 0;
                }
                else {
                    continue;
                }
            }
            else if (state[i] % 2 == playerCode % 2 && state[i] < 7) {
                // This field can be activated.
                auto newMove = Mov(state, i, j);
                if (newMove.second) {
                    arr.emplace_back(i, j, alphabeta(newMove.first, 1, 0, selector,
                                                     -100000, 100000, false));
                }
            }
            else {
                // This field can't be activated (it's solid or occupied by the enemy player).
                break;
            }
        }
    }

    if (passCounter == 0) {
        arr.emplace_back(-1, 0, alphabeta(state, 1, 1, selector,
                                          -100000, 100000, false));
    }
    else if (passCounter == 1) {
        int score = EvaluateEnd(state, selector);
        if (score > 0) {
            return std::make_pair<int, int>(-1, 0);
        }

        arr.emplace_back(-1, 0, score < 0 ? -90000 : 0);
    }

    int maximum = std::get<2>(arr[0]);

    for (const auto& a : arr) {
        if (std::get<2>(a) > maximum) {
            maximum = std::get<2>(a);
        }
    }

    std::vector<MinMaxMove> retArr;

    for (const auto& a : arr) {
        if (std::get<2>(a) == maximum) {
            retArr.push_back(a);
        }
    }

    // Increase depth after some turns
    /*counter++;
    if (counter == maxCounter) {
        counter = 0;
        maxCounter = -1;
        depth += 1;
    }*/

    auto toReturn = retArr[0];

    if (retArr.size() > 2) {
        std::vector<MinMaxMove> toReturnArray;
        std::sample(
                retArr.begin(),
                retArr.end(),
                std::back_inserter(toReturnArray),
                1,
                std::mt19937{std::random_device{}()}
        );
        toReturn = toReturnArray[0];
    }

    return std::make_pair<int, int>((int)std::get<0>(toReturn),
                                    (int)std::get<1>(toReturn));
}

double MinMax::alphabeta(std::vector<int> state,
        int d, int passCounter, std::vector<int> selector, double alpha, double beta, bool maximizing) {

    if (d >= depth) {
        return Evaluate(state, selector, d);
    }

    double v = 0;

    if (maximizing) {
        v = -100000;
        if (EvaluateExt(state, selector) < alpha) {
            return v;
        }
        if (passCounter == 0) {
            v = std::max(v, alphabeta(state, d+1, 1, selector,
                    alpha, beta, false));
            alpha = std::max(alpha, v);
            if (beta <= alpha) {
                return v;
            }
        }
        else if (passCounter == 1) {
            double score = EvaluateEnd(state, selector);
            if (score > 0) {
                return 90000 + d;
            }
            else if (score == 0) {
                v = std::max(v, (double) d);
            }
            else {
                v = std::max(v, -90000.0 + d);
            }
            alpha = std::max(alpha, v);
            if (beta <= alpha) {
                return v;
            }
        }

        for (int i = 0; i < 16; i++) {
            bool shouldBreak = false;
            for (int j = 0; j<4; j++) {
                if (state[i] == 0) {
                    int x = j*2 + playerCode - 1;
                    if (selector[x] > 0) {
                        state[i] = x+1;
                        selector[x] -= 1;
                        v = std::max(v, alphabeta(state, d+1, 0, selector,
                                alpha, beta, false));
                        alpha = std::max(alpha, v);
                        selector[x] += 1;
                        state[i] = 0;
                        if (beta <= alpha) {
                            shouldBreak = true;
                            break;
                        }
                    }
                    else {
                        continue;
                    }
                }
                else if (state[i] % 2 == playerCode % 2 && state[i] < 7) {
                    // This field can be activated.
                    auto newMove = Mov(state, i, j);
                    if (newMove.second) {
                        v = std::max(v, alphabeta(newMove.first, d+1, 0,
                                                  selector, alpha, beta, false));
                        alpha = std::max(alpha, v);
                        if (beta <= alpha) {
                            shouldBreak = true;
                            break;
                        }
                    }
                }

            }
            if (shouldBreak) {
                break;
            }
        }
    }
    else {
        v = 100000;

        if (passCounter == 0) {
            v = std::min(v, alphabeta(state, d+1, 1, selector,
                    alpha, beta, true));
            beta = std::min(beta, v);
            if (beta <= alpha) {
                return v;
            }
        } else if (passCounter == 1) {
            double score = EvaluateEnd(state, selector);
            if (score < 0) {
                return -90000 + d;
            }
            else if (score == 0) {
                v = std::min(v,(double)d);
            }
            else {
                v = std::min(v, 90000.0+d);
            }
            beta = std::min(beta, v);
            if (beta <= alpha) {
                return v;
            }
        }

        for (int i = 0; i < 16; i++) {
            bool shouldBreak = false;
            for (int j = 0; j<4; j++) {
                if (state[i] == 0) {
                    int x = j*2 + playerCode % 2;
                    if (selector[x] > 0) {
                        state[i] = x+1;
                        selector[x] -= 1;
                        v = std::min(v, alphabeta(state, d+1, 0, selector, alpha, beta, true));
                        beta = std::min(beta, v);
                        selector[x] += 1;
                        state[i] = 0;
                        if (beta <= alpha) {
                            shouldBreak = true;
                            break;
                        }
                    }
                    else {
                        continue;
                    }
                }
                else if (state[i] % 2 == (playerCode-1) % 2 && state[i] < 7) {
                    // This field can be activated.
                    auto newMove = Mov(state, i, j);
                    if (newMove.second) {
                        v = std::min(v, alphabeta(newMove.first, d+1, 0,
                                                  selector, alpha, beta, true));
                        beta = std::min(beta, v);
                        if (beta <= alpha) {
                            shouldBreak = true;
                            break;
                        }
                    }
                }

            }
            if (shouldBreak) {
                break;
            }
        }
    }

    return v;
}

int MinMax::EvaluateExt(const std::vector<int> &state, const std::vector<int> &selector) {
    int score = 0;

    for (const auto& field : state) {
        if (field == 0) {
            continue;
        }
        if (field % 2 == playerCode % 2) {
            score += 100;
        }
        else {
            score -= 100;
        }
    }

    for (int s = playerCode - 1; s < 8; s += 2) {
        score += 100 * selector[s];
    }
    for (int s = playerCode % 2; s < 8; s += 2) {
        score += 100 * selector[s];
    }

    return score;
}

int MinMax::Evaluate(const std::vector<int> &state, const std::vector<int> &selector, int d) {
    int score = 0;
    std::vector<int> arr = {0,0,0,0,0,0,0,0};
    std::vector<int> selectorBase = {5,5,1,1,1,1,1,1};

    for (const auto& field : state) {
        if (field == 0) {
            continue;
        }
        if (field % 2 == playerCode % 2) {
            score += 100;
            arr[field - 1] +=1;
        }
        else {
            score -= 100;
            arr[field - 1] += 1;
        }
    }

    for (int i = 0; i < selector.size(); ++i) {
        score += (int) pow(-1.0, i + playerCode) * 1000 * (selectorBase[i] - arr[i] - selector[i]);
    }
    return score + d;
}

int MinMax::EvaluateEnd(const std::vector<int> &state, const std::vector<int> &selector) {
    int score = 0;

    for (const auto& field : state) {
        if (field == 0) {
            continue;
        }
        if (field % 2 == playerCode % 2) {
            score += 100;
        }
        else {
            score -= 100;
        }
    }

    return score;
}

std::pair<std::vector<int>, bool> MinMax::Mov(const std::vector<int> &state, int field, int rot) {
    if (state[field] == 0) {
        return std::make_pair<std::vector<int>, bool>(std::vector<int>(state), false);
    }

    std::vector newState(state);
    std::vector<int> directions;
    directions.push_back(rot);
    if (state[field] == 3 || state[field] == 4) {
        directions.push_back((rot+3) % 4);
    }
    else if (state[field] == 5 || state[field] == 6) {
        directions.push_back((rot+2) % 4);
    }

    for (int dir : directions) {
        if (dir == 0 && field<12 && (state[field+4]>=7 ||
            (field>=8 && state[field+4] && state[field+4]%2 == state[field]%2) ||
            (state[field+4] && field<8 && state[field+8]))) {
            return std::make_pair<std::vector<int>, bool>(std::vector<int>(state), false);
        }

        if (dir == 1 && (field+1)%4!=0 && (state[field+1]>=7 ||
            ((field+2)%4==0 && state[field+1] && state[field+1]%2 == state[field]%2) ||
            (state[field+1] && (field+2)%4!=0 && state[field+2]))) {
            return std::make_pair<std::vector<int>, bool>(std::vector<int>(state), false);
        }
        if (dir == 2 && field>=4 && (state[field-4]>=7 ||
            (field<8 && state[field-4] && state[field-4]%2 == state[field]%2) ||
            (state[field-4] && field>=8 && state[field-8]))) {
            return std::make_pair<std::vector<int>, bool>(std::vector<int>(state), false);
        }
        if (dir == 3 && field%4!=0 && (state[field-1]>=7 ||
            ((field-1)%4==0 && state[field-1] && state[field-1]%2 == state[field]%2) ||
            (state[field-1] && (field-1)%4!=0 && state[field-2]))) {
            return std::make_pair<std::vector<int>, bool>(std::vector<int>(state), false);
        }
    }

    bool canMove = false;
    for (int dir : directions) {
        if (dir==0 && field<12 && state[field+4]) { // 0 -> down
            canMove = true;
            if (field < 8) {
                newState[field+8] = newState[field+4];
            }
            newState[field+4] = 0;
        } else if (dir == 1 && (field+1)%4!=0 && state[field+1]) { // 1 -> right
            canMove = true;
            if ((field+2)%4!=0) {
                newState[field + 2] = newState[field + 1];
            }
            newState[field + 1] = 0;
        } else if (dir == 2 && field >= 4 && state[field-4]) { // 2 -> up
            canMove = true;
            if (field >= 8) {
                newState[field-8] = newState[field-4];
            }
            newState[field-4] = 0;
        } else if (dir == 3 && field%4!=0 && state[field-1]) { // 3 -> left
            canMove = true;
            if ((field-1)%4!=0) {
                newState[field-2] = newState[field-1];
            }
            newState[field-1] = 0;
        }
    }

    return std::pair<std::vector<int>, bool>(newState, canMove);
}
