//
// Created by bartek on 22.10.2019.
//

#ifndef PISTONED_PISTON_H
#define PISTONED_PISTON_H


#include <TTH/Core/BehaviourComponent.h>
#include <TTH/Core/ComponentFactory.h>
#include "BoardController.h"

class Piston : public TTH::BehaviourComponent{
public:
    void Start() override;

    void Update(float dt) override;

    void OnMouseDown(int button) override;

    void OnLeave() override;

    Piston *Clone() const override;

    bool LoadFromFile(TTH::IniFile &file) override;

    int num;

    void InitArms();
    void Rotate();
    void Activate();

    // For AI only
    void RotateAndActivate(int rotateTo);
    int rotActAICounter;

    void Delete();

    void Move(BoardController::Direction direction);

    float getTargetRotation() const;
    const glm::vec3 &getTargetPosition() const;

    BoardController::Direction direction;

    enum class Type{
        Front = 0, FrontRight = 1, FrontBack = 2, None = 3
    };
    Type type;

    BoardController::ActivePlayer owner;
private:
    enum class State{
        Idle, Rotating, MovingUp, MovingDown, Expanding, Contracting, Moving
    };

    State state;

    /*TTH::Transform* arm;
    TTH::Transform* end;
    TTH::Object* armObj;
    TTH::Object* endObj;*/

    std::vector<std::pair<TTH::Object*, TTH::Object*>> arms;

    float targetRotation;
    glm::vec3 targetPosition;

    bool isDeleted;

    static float rotationVelocity;
    float rotationDirection;

    static TTH::ComponentRegister<Piston> reg;

    void OnLoad() override;
};


#endif //PISTONED_PISTON_H
