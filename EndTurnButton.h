//
// Created by bartek on 04.12.2019.
//

#ifndef PISTONED_ENDTURNBUTTON_H
#define PISTONED_ENDTURNBUTTON_H

#include <TTH/Core/BehaviourComponent.h>
#include <TTH/Core/ComponentFactory.h>
#include <TTH/UI/SpriteComponent.h>

class EndTurnButton : public TTH::BehaviourComponent{
public:
    void Start() override;
    void Update(float dt) override;
    void OnLeave() override;
    void OnMouseDown(int button) override;
    void OnMouseEnter() override;
    void OnMouseLeave() override;

    EndTurnButton *Clone() const override;

    void Switch();
    void Reset();

protected:
    bool LoadFromFile(TTH::IniFile &file) override;

private:
    static TTH::ComponentRegister<EndTurnButton> reg;

    bool clickedOnce;

    TTH::SpriteComponent* sprite;
};


#endif //PISTONED_ENDTURNBUTTON_H
