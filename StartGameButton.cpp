//
// Created by bartek on 05.12.2019.
//

#include <TTH/SceneManagement/SceneManager.h>
#include "StartGameButton.h"

TTH::ComponentRegister<StartGameButton> StartGameButton::reg("StartGameButton");

void StartGameButton::Start() {
    sprite = parent->GetComponent<TTH::SpriteComponent>();
}

void StartGameButton::Update(float dt) {

}

void StartGameButton::OnLeave() {

}

void StartGameButton::OnMouseDown(int button) {
    if(aiGame){
        TTH::SceneManager::LoadNextSceneRequest("AIGame");
    } else {
        TTH::SceneManager::LoadNextSceneRequest("Game");
    }
}

void StartGameButton::OnMouseEnter() {
    sprite->SetColorTint(glm::vec3(1.0f,0.5f,0.0f));
}

void StartGameButton::OnMouseLeave() {
    sprite->SetColorTint(glm::vec3(1.0f,1.0f,0.0f));
}

TTH::Component *StartGameButton::Clone() const {
    return nullptr;
}

bool StartGameButton::LoadFromFile(TTH::IniFile &file) {
    file.SetToSection("StartGameButton");
    file.RequireValue("ai", aiGame);
    return true;
}
