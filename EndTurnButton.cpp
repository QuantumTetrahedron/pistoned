//
// Created by bartek on 04.12.2019.
//

#include <TTH/Core/ObjectManager.h>
#include "EndTurnButton.h"
#include "BoardController.h"

TTH::ComponentRegister<EndTurnButton> EndTurnButton::reg("EndTurnButton");

void EndTurnButton::Start() {
    sprite = parent->GetComponent<TTH::SpriteComponent>();
    clickedOnce = false;
}

void EndTurnButton::Update(float dt) {
}

void EndTurnButton::OnLeave() {

}

void EndTurnButton::OnMouseDown(int button) {
    if(button == 0)
        BoardController::getInstance()->PassTurn();
}

void EndTurnButton::OnMouseEnter() {
    if(clickedOnce){
        sprite->SetColorTint(glm::vec3(1.0f, 0.2f, 0.0f));
    } else {
        sprite->SetColorTint(glm::vec3(1.0f, 0.5f, 0.0f));
    }

}

void EndTurnButton::OnMouseLeave() {
    if(clickedOnce){
        sprite->SetColorTint(glm::vec3(1.0f, 0.5f, 0.0f));
    } else {
        sprite->SetColorTint(glm::vec3(1.0f, 1.0f, 0.0f));
    }
}

EndTurnButton *EndTurnButton::Clone() const {
    return nullptr;
}

bool EndTurnButton::LoadFromFile(TTH::IniFile &file) {
    return true;
}

void EndTurnButton::Switch() {
    clickedOnce = true;
    sprite->SetColorTint(glm::vec3(1.0f, 0.5f, 0.0f));
}

void EndTurnButton::Reset() {
    clickedOnce = false;
    sprite->SetColorTint(glm::vec3(1.0f, 1.0f, 0.0f));
}
