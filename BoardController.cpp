//
// Created by bartek on 20.10.2019.
//

#include <TTH/Core/ObjectFactory.h>
#include <TTH/Core/ObjectManager.h>
#include <TTH/SceneManagement/SceneManager.h>
#include <TTH/Input/Input.h>
#include "BoardController.h"
#include "Piston.h"

TTH::ComponentRegister<BoardController> BoardController::reg("Board");

BoardController* BoardController::instance = nullptr;

void BoardController::Start() {
    activePlayer = ActivePlayer::P1;
    player1.init(TTH::ObjectManager::GetObject("P1Selector")->GetComponent<Selector>());
    player2.init(TTH::ObjectManager::GetObject("P2Selector")->GetComponent<Selector>());

    player1.Activate();
    player2.Deactivate();
    passed = false;

    for(int i = 0; i<4; i++){
        for(int j = 0; j<4;j++){
            TTH::IniFile file;
            file.ReadFile("Archetypes/Field.ini");
            TTH::ObjectFactory::SetArchetype(file);
            std::string fieldName = "field"+std::to_string(i*4+j);
            auto obj = TTH::ObjectFactory::Create(file, fieldName);
            auto field = obj->GetComponent<Field>();
            field->num = i*4+j;
            fields.push_back(field);
            obj->transform.position = glm::vec3(2.0f*((float)j-1.5f), 0, 2.0f*((float)i-1.5f));
            //obj->SetParent(parent);
            TTH::ObjectManager::AddObject(obj);
        }
    }

    state = State::Idle;

    endTurnButton = TTH::ObjectManager::GetObject("PassButton")->GetComponent<EndTurnButton>();

    if (aiGame) {
        minmax = std::make_unique<MinMax>(2); // BoardController::ActivePlayer::P2
    }

    instance = this;
}

void BoardController::Update(float dt) {
    if(state == State::AITurn){
        simulateAITurn();
    }

    if(TTH::Input::GetButton("Escape")){
        TTH::SceneManager::PauseAndLoadSceneRequest("Pause");
    }
}

void BoardController::simulateAITurn() {
    std::pair<int,int> p = minmax->GetMove(fields,
            passed ? 1 : 0, player1.getSelector(), player2.getSelector());

    state = State::AITurnExecuting;

    if(p.first >= 0) {
        if (fields[p.first]->piston == nullptr) {
            player2.getSelector().Select(p.second);
            Place(p.first, p.second);
        } else {
            //Activate(fields[p.first]->piston);
            fields[p.first]->piston->RotateAndActivate(p.second);
        }
    } else {
        PassTurn();
    }
}

void BoardController::OnLeave() {
    if(instance == this) instance = nullptr;
}

BoardController *BoardController::Clone() const {
    return nullptr;
}

bool BoardController::LoadFromFile(TTH::IniFile &file) {
    file.SetToSection("Board");
    file.RequireValue("ai", aiGame);
    return true;
}

void BoardController::Activate(Piston* piston) {

    Direction direction = piston->direction;
    Piston::Type type = piston->type;
    if(type != Piston::Type::None && (state == State::Idle || state == State::AITurnExecuting) && piston->owner == activePlayer) {

        std::vector<Direction> dirs;
        if(type == Piston::Type::Front){
            dirs.push_back(piston->direction);
        } else if(type == Piston::Type::FrontRight){
            dirs.push_back(direction);
            dirs.push_back(rotateRight(direction));
        } else if(type == Piston::Type::FrontBack){
            dirs.push_back(direction);
            dirs.push_back(rotateRight(rotateRight(direction)));
        }

        bool blocked = false;

        for(Direction dir : dirs) {
            int num = piston->num;

            int targetNum = num;
            int destNum = num;
            bool offScreen = false;

            if (dir == Direction::Up) {
                targetNum -= 4;
                destNum = targetNum - 4;
                offScreen = targetNum < 0;
            } else if (dir == Direction::Left) {
                offScreen = num % 4 == 0;
                targetNum -= 1;
                destNum = targetNum - 1;
            } else if (dir == Direction::Right) {
                targetNum += 1;
                destNum = targetNum + 1;
                offScreen = targetNum % 4 == 0;
            } else if (dir == Direction::Down) {
                targetNum += 4;
                destNum = targetNum + 4;
                offScreen = targetNum > 15;
            }
            bool destroy = destNum < 0 || destNum > 15 || glm::abs((destNum % 4) - (targetNum % 4)) > 1;

            bool b = !offScreen && fields[targetNum]->piston != nullptr && ( (!destroy && fields[destNum]->piston != nullptr) || fields[targetNum]->piston->type == Piston::Type::None);

            blocked = blocked || b;
        }

        if(!blocked){
            bool pushed = false;
            for(Direction dir : dirs) {
                int num = piston->num;

                int targetNum = num;
                int destNum = num;
                bool offScreen = false;

                if (dir == Direction::Up) {
                    targetNum -= 4;
                    destNum = targetNum - 4;
                    offScreen = targetNum < 0;
                } else if (dir == Direction::Left) {
                    offScreen = num % 4 == 0;
                    targetNum -= 1;
                    destNum = targetNum - 1;
                } else if (dir == Direction::Right) {
                    targetNum += 1;
                    destNum = targetNum + 1;
                    offScreen = targetNum % 4 == 0;
                } else if (dir == Direction::Down) {
                    targetNum += 4;
                    destNum = targetNum + 4;
                    offScreen = targetNum > 15;
                }
                bool destroy = destNum < 0 || destNum > 15 || glm::abs((destNum % 4) - (targetNum % 4)) > 1;

                bool pushing = !offScreen;

                std::cout << num << " " << targetNum << std::endl;
                std::cout << (pushing ? ":D" : ":(") << std::endl;
                pushing = pushing && fields[targetNum]->piston != nullptr;
                std::cout << (pushing ? ":D" : ":(") << std::endl;

                pushed = pushed || pushing;
                if (pushing) {
                    fields[targetNum]->piston->Move(dir);
                    if (!destroy) {
                        fields[destNum]->piston = fields[targetNum]->piston;
                        fields[destNum]->piston->num = destNum;
                        fields[targetNum]->piston = nullptr;
                    } else {
                        auto p = fields[targetNum]->piston;
                        p->Delete();
                        fields[targetNum]->piston = nullptr;
                    }
                }
            }

            state = State::Animating;
            piston->Activate();

            if(pushed){
                EndTurn();
            }
        }
    }
}

void BoardController::Rotate(Piston* piston) {
    if((state == State::Idle || state == State::AITurnExecuting) && piston->owner == activePlayer) {
        state = State::Animating;
        fields[piston->num]->piston->Rotate();
    }
}

void BoardController::Place(int num, int type) {
    if((state == State::Idle || state == State::AITurnExecuting) && fields[num]->piston == nullptr) {
        Player* player = getActivePlayer();
        if(player->GetSelectedCount() == 0) return;

        player->UseSelected();

        TTH::IniFile file;
        file.ReadFile("Archetypes/Piston.ini");
        TTH::ObjectFactory::RemoveArchetype();
        std::string fieldName = "field" + std::to_string(num) + "_piston";
        auto obj = TTH::ObjectFactory::Create(file, fieldName);
        obj->transform.position = fields[num]->GetPosition() + glm::vec3(0.0, 0.2, 0.0);

        auto rc = obj->GetComponent<TTH::RenderComponent>();
        std::string modelName = "Piston";
        modelName += std::to_string(type);
        modelName += activePlayer == ActivePlayer::P1 ? "X" : "O";
        rc->SetModel(modelName);

        auto piston = obj->GetComponent<Piston>();
        piston->num = num;
        piston->type = (Piston::Type)type;
        piston->owner = activePlayer;
        TTH::ObjectManager::AddObject(obj);
        piston->InitArms();
        fields[num]->piston = piston;

        EndTurn();

        if(aiGame && activePlayer == ActivePlayer::P2){
            state = State::AITurn;
        } else {
            state = State::Idle;
        }
    }
}

void BoardController::ActivationEnd() {

    if(aiGame && activePlayer == ActivePlayer::P2){
        state = State::AITurn;
    } else {
        state = State::Idle;
    }
}

void BoardController::RotationEnd() {
    if(aiGame && activePlayer == ActivePlayer::P2){
        state = State::AITurnExecuting;
    } else {
        state = State::Idle;
    }
}

BoardController::Direction BoardController::rotateRight(BoardController::Direction dir) {
    if(dir == Direction::Right){
        return Direction::Down;
    } else if(dir == Direction::Down){
        return Direction::Left;
    } else if(dir == Direction::Left){
        return Direction::Up;
    } else if(dir == Direction::Up){
        return Direction::Right;
    }
}

BoardController::Direction BoardController::rotateLeft(BoardController::Direction dir) {
    if(dir == Direction::Right){
        return Direction::Up;
    } else if(dir == Direction::Down){
        return Direction::Right;
    } else if(dir == Direction::Left){
        return Direction::Down;
    } else if(dir == Direction::Up){
        return Direction::Left;
    }
}

Player *BoardController::getActivePlayer() {
    if(activePlayer == ActivePlayer::P1){
        return &player1;
    } else {
        return &player2;
    }
}

void BoardController::EndTurn() {
    passed = false;
    endTurnButton->Reset();

    activePlayer = activePlayer == ActivePlayer::P1 ? ActivePlayer::P2 : ActivePlayer::P1;
    if(activePlayer == ActivePlayer::P1){
        player1.Activate();
        player2.Deactivate();
    } else {
        player1.Deactivate();
        player2.Activate();
    }

}

void BoardController::PassTurn() {
    if(!passed) {
        passed = true;
        endTurnButton->Switch();

        activePlayer = activePlayer == ActivePlayer::P1 ? ActivePlayer::P2 : ActivePlayer::P1;
        if (activePlayer == ActivePlayer::P1) {
            player1.Activate();
            player2.Deactivate();
        } else {
            player1.Deactivate();
            player2.Activate();
        }

        if(aiGame && activePlayer == ActivePlayer::P2){
            state = State::AITurn;
        } else {
            state = State::Idle;
        }

    } else {
        // End game
        int p1score = 0, p2score = 0;
        for(auto f : fields){
            if(f->piston != nullptr){
                if(f->piston->owner == ActivePlayer::P1){
                    p1score++;
                } else {
                    p2score++;
                }
            }
        }
        if(p1score > p2score){
            TTH::SceneManager::LoadNextSceneRequest("P1WinScreen");
        } else if(p1score < p2score){
            TTH::SceneManager::LoadNextSceneRequest("P2WinScreen");
        } else {
            TTH::SceneManager::LoadNextSceneRequest("DrawScreen");
        }
    }
}

BoardController *BoardController::getInstance() {
    return instance;
}
