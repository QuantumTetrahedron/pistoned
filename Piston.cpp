//
// Created by bartek on 22.10.2019.
//

#include <TTH/Core/ObjectManager.h>
#include <TTH/Core/ObjectFactory.h>
#include <TTH/Input/Input.h>
#include "Piston.h"
#include "BoardController.h"

TTH::ComponentRegister<Piston> Piston::reg("Piston");

float Piston::rotationVelocity = 360.0f;

void Piston::Start() {
    isDeleted = false;
    targetRotation = parent->transform.rotation.y;
    state = State::Idle;
    direction = BoardController::Direction::Right;
    type = Type::Front;
    rotActAICounter = 0;
}

void Piston::Update(float dt) {
    if(state == State::Idle){
        if(rotActAICounter > 1){
            BoardController::getInstance()->Rotate(this);
            rotActAICounter--;
        } else if(rotActAICounter == 1){
            BoardController::getInstance()->Activate(this);
            rotActAICounter--;
        }
    }


    if(state == State::MovingUp){
        float posY = parent->transform.position.y;
        float offset = 2*dt;
        if(glm::abs(posY - 1) < offset){
            posY = 1;
            state = State::Rotating;
        } else {
            posY += 2*dt;
        }
        parent->transform.position.y = posY;
    } else if(state == State::Rotating) {
        float rot = parent->transform.rotation.y;
        float offset = dt * rotationVelocity;
        if (glm::abs(rot - targetRotation) < offset) {
            rot = targetRotation;
            rot = glm::mod(rot, 360.0f);
            state = State::MovingDown;

            direction = BoardController::rotateLeft(direction);
        } else {
            rot += rotationDirection * offset;
        }
        parent->transform.rotation.y = rot;
    } else if(state == State::MovingDown){
        float posY = parent->transform.position.y;
        float offset = 2*dt;
        if(glm::abs(posY - 0.2) < offset){
            posY = 0.2;
            state = State::Idle;
            BoardController::getInstance()->RotationEnd();
        } else {
            posY -= 2*dt;
        }
        parent->transform.position.y = posY;
    } else if(state == State::Expanding){
        if(type != Type::None) {
            auto arm = &arms[0].first->transform;
            auto end = &arms[0].second->transform;

            float posX = end->position.x;
            float offset = 8 * dt;
            if (glm::abs(posX - 2.0f) < offset) {
                posX = 2.0f;
                state = State::Contracting;
            } else {
                posX += 8 * dt;
            }
            end->position.x = posX;
            arm->scale.x = 1.0f + (5.0f * posX) / 4.0f;
        }
        if(type == Type::FrontRight){
            auto arm = &arms[1].first->transform;
            auto end = &arms[1].second->transform;

            float posZ = end->position.z;
            float offset = 8 * dt;
            if (glm::abs(posZ - 2.0f) < offset) {
                posZ = 2.0f;
            } else {
                posZ += 8 * dt;
            }
            end->position.z = posZ;
            arm->scale.x = 1.0f + (5.0f * posZ) / 4.0f;
        } else if(type == Type::FrontBack){
            auto arm = &arms[1].first->transform;
            auto end = &arms[1].second->transform;

            float posX = end->position.x;
            float offset = 8 * dt;
            if (glm::abs(posX + 2.0f) < offset) {
                posX = -2.0f;
            } else {
                posX -= 8 * dt;
            }
            end->position.x = posX;
            arm->scale.x = 1.0f + (5.0f * -posX) / 4.0f;
        }
    } else if(state == State::Contracting){
        if(type != Type::None) {
            auto arm = &arms[0].first->transform;
            auto end = &arms[0].second->transform;

            float posX = end->position.x;
            float offset = 8 * dt;
            if (glm::abs(posX) < offset) {
                posX = 0.0f;
                state = State::Idle;
                BoardController::getInstance()->ActivationEnd();
            } else {
                posX -= 8 * dt;
            }
            end->position.x = posX;
            arm->scale.x = 1.0f + (5.0f * posX) / 4.0f;
        }
        if(type == Type::FrontRight){
            auto arm = &arms[1].first->transform;
            auto end = &arms[1].second->transform;

            float posZ = end->position.z;
            float offset = 8 * dt;
            if (glm::abs(posZ) < offset) {
                posZ = 0.0f;
            } else {
                posZ -= 8 * dt;
            }
            end->position.z = posZ;
            arm->scale.x = 1.0f + (5.0f * posZ) / 4.0f;
        } else if(type == Type::FrontBack){
            auto arm = &arms[1].first->transform;
            auto end = &arms[1].second->transform;

            float posX = end->position.x;
            float offset = 8 * dt;
            if (glm::abs(posX) < offset) {
                posX = 0.0f;
            } else {
                posX += 8 * dt;
            }
            end->position.x = posX;
            arm->scale.x = 1.0f + (5.0f * -posX) / 4.0f;
        }
        /*
        for(auto p : arms){
            auto arm = &p.first->transform;
            auto end = &p.second->transform;

            float posX = end->position.x;
            float offset = 8*dt;
            if(glm::abs(posX) < offset){
                posX = 0.0f;
                state = State::Idle;
                BoardController::ActivationEnd();
            } else {
                posX -= 8*dt;
            }
            end->position.x = posX;
            arm->scale.x = 1.0f + (5.0f * posX) / 4.0f;
        }*/
    } else if(state == State::Moving){
        glm::vec3 pos = parent->transform.position;
        glm::vec3 dir = targetPosition - pos;
        if(glm::length(dir) < 8*dt){
            pos = targetPosition;
            state = State::Idle;

        } else {
            pos += glm::normalize(dir) * 8.0f * dt;
        }
        parent->transform.position = pos;
    } else if(state == State::Idle){
        if(isDeleted){
            parent->Kill();
            for(auto p : arms){
                p.first->Kill();
                p.second->Kill();
            }
        }
    }
}

void Piston::OnLeave() {

}

Piston *Piston::Clone() const {
    return nullptr;
}

bool Piston::LoadFromFile(TTH::IniFile &file) {
    return true;
}

void Piston::OnMouseDown(int button) {
    if(button == 1 && state == State::Idle) {
        BoardController::getInstance()->Rotate(this);
    } else if(button == 0 && state == State::Idle){
        BoardController::getInstance()->Activate(this);
    }
}

void Piston::OnLoad() {
    Start();
}

void Piston::Rotate() {
    targetRotation = parent->transform.rotation.y + 90.0f;

    float less = targetRotation - 360.0f;
    float more = targetRotation + 360.0f;

    float rot = parent->transform.rotation.y;

    float rDist = glm::abs(rot - targetRotation);
    float lDist = glm::abs(rot - less);
    float mDist = glm::abs(rot - more);

    if (rDist < lDist && rDist < mDist) {
        // rotate to target
    } else if (lDist < mDist) {
        // rotate to less
        targetRotation = less;
    } else {
        // rotate to more
        targetRotation = more;
    }
    rotationDirection = glm::sign(targetRotation - rot);
    state = State::MovingUp;
}

void Piston::Activate() {
    state = State::Expanding;
}

void Piston::Move(BoardController::Direction d) {
    if(d == BoardController::Direction::Up){
        targetPosition = parent->transform.position + glm::vec3(0.0f,0.0f,-2.0f);
    } else if(d == BoardController::Direction::Down){
        targetPosition = parent->transform.position + glm::vec3(0.0f,0.0f,2.0f);
    } else if(d == BoardController::Direction::Left){
        targetPosition = parent->transform.position + glm::vec3(-2.0f,0.0f,0.0f);
    } else if(d == BoardController::Direction::Right){
        targetPosition = parent->transform.position + glm::vec3(2.0f,0.0f,0.0f);
    }
    state = State::Moving;
}

void Piston::Delete() {
    isDeleted = true;
}

void Piston::InitArms() {

    TTH::IniFile file;
    file.ReadFile("Archetypes/Arm.ini");

    TTH::IniFile file2;
    file2.ReadFile("Archetypes/BasicEnd.ini");

    if(type != Type::None){
        auto armObj = TTH::ObjectFactory::Create(file, parent->GetName() + "_arm1");
        armObj->SetParent(parent);
        TTH::ObjectManager::AddObject(armObj);

        auto endObj = TTH::ObjectFactory::Create(file2, parent->GetName()+"_end1");
        endObj->SetParent(parent);
        TTH::ObjectManager::AddObject(endObj);

        arms.emplace_back(armObj, endObj);
    }
    if(type == Type::FrontRight){
        auto armObj = TTH::ObjectFactory::Create(file, parent->GetName() + "_arm2");
        armObj->SetParent(parent);
        armObj->transform.rotation.y = 270.0f;
        TTH::ObjectManager::AddObject(armObj);

        auto endObj = TTH::ObjectFactory::Create(file2, parent->GetName()+"_end2");
        endObj->SetParent(parent);
        endObj->transform.rotation.y = 270.0f;
        TTH::ObjectManager::AddObject(endObj);

        arms.emplace_back(armObj, endObj);
    } else if(type == Type::FrontBack){

        auto armObj = TTH::ObjectFactory::Create(file, parent->GetName() + "_arm2");
        armObj->SetParent(parent);
        armObj->transform.rotation.y = 180.0f;
        TTH::ObjectManager::AddObject(armObj);

        auto endObj = TTH::ObjectFactory::Create(file2, parent->GetName()+"_end2");
        endObj->SetParent(parent);
        endObj->transform.rotation.y = 180.0f;
        TTH::ObjectManager::AddObject(endObj);

        arms.emplace_back(armObj, endObj);
    }
}

void Piston::RotateAndActivate(int rotateTo) {
    auto dir = (BoardController::Direction)rotateTo;
    if(dir == direction){
        rotActAICounter = 1;
    } else if(dir == BoardController::rotateLeft(direction)){
        rotActAICounter = 2;
    } else if(dir == BoardController::rotateLeft(BoardController::rotateLeft(direction))){
        rotActAICounter = 3;
    } else{
        rotActAICounter = 4;
    }
}

float Piston::getTargetRotation() const {
    return targetRotation;
}

const glm::vec3 &Piston::getTargetPosition() const {
    return targetPosition;
}
