#include <iostream>

#include <TTH/Core/Engine.h>

int main() {
    TTH::Game::Initialize("GameData/EngineOptions.ini");
    TTH::Game::MainLoop();
    TTH::Game::Shutdown();
    return 0;
}