//
// Created by bartek on 05.12.2019.
//

#ifndef PISTONED_CHANGESCENEBUTTON_H
#define PISTONED_CHANGESCENEBUTTON_H


#include <TTH/Core/BehaviourComponent.h>

class ChangeSceneButton : public TTH::BehaviourComponent{
public:
    void Start() override;

    void Update(float dt) override;

    void OnLeave() override;

    void OnMouseDown(int button) override;

    void OnMouseEnter() override;

    void OnMouseLeave() override;

    Component *Clone() const override;

protected:
    bool LoadFromFile(TTH::IniFile &file) override;

private:
    TTH::SpriteComponent* sprite;
    std::string scene;

    static TTH::ComponentRegister<ChangeSceneButton> reg;
};


#endif //PISTONED_CHANGESCENEBUTTON_H
