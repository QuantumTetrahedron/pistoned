//
// Created by bartek on 06.12.2019.
//

#ifndef PISTONED_TEXT_H
#define PISTONED_TEXT_H


#include <TTH/Core/BehaviourComponent.h>
#include <TTH/Core/ComponentFactory.h>

class Text : public TTH::BehaviourComponent{
public:
    static float scale;
    float currScale;

    void Start() override;

    void Update(float dt) override;

    void OnLeave() override;

    Component *Clone() const override;

protected:
    bool LoadFromFile(TTH::IniFile &file) override;

    static TTH::ComponentRegister<Text> reg;
};


#endif //PISTONED_TEXT_H
